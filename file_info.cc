// -*- mode: c++ -*-

#include <boost/network/protocol/http/client.hpp>
#include <boost/network/uri.hpp>
#include <boost/lexical_cast.hpp>

#include "file_info.h"

namespace http = boost::network::http;
namespace uri = boost::network::uri;

file_info::file_info( const std::string& remote )
    : m_size(0)
{
    try
    {
        http::client client;
        http::client::request request( remote );

        std::string path = uri::path( request.uri() );
        std::size_t index = path.find_last_of( '/' );
        std::string filename = path.substr( index + 1 );

        m_filename = ( filename.empty() ? "index.html" : filename );

        // try to get the headers of the remote content
        // determine the total file size
        // note that some http server sends bogus Content-Length
        // here I assume the responded header is always accurate
        request << boost::network::header( "Connection", "close" ); // don't transfer any content yet
        http::client::response response = client.head( request );

        http::client::response::headers_container_type hcon = headers( response );

        http::client::response::headers_container_type::const_iterator iter = hcon.find( "Content-Length" );
        if ( iter != hcon.end() )
            m_size = boost::lexical_cast<size_t>( iter->second );
    }
    catch ( std::exception& e )
    {
        std::cerr << e.what() << std::endl;
        throw; // failed to contruct the file_info object, so we should let others know this object is incomplete
    }
}
