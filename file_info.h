// -*- mode: c++ -*-

#ifndef FILE_INFO_H
#define FILE_INFO_H

#include <string>
// #include <iostream>

/*
  Holds the meta info of a remote file.

  For now, it only contains the file size.
*/
class file_info
{
public:
    file_info( const std::string& remote_file );

    const size_t get_file_size() const { return m_size; }

    // get normalized file name from URI
    std::string get_file_name() const { return m_filename; }

private:
    std::string m_filename;
    size_t m_size;
};

#endif
