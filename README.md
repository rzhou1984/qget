# README #

qget - A simple download booster

### What is this repository for? ###

* Quick Summary

    qget is a tool for downloading HTTP files in multiple chunks.

    This is a trial project for Carnegie Coding Check.

* Version

    0.1

### How do I get set up? ###

* Dependencies

    `gcc >= 4.8`

    `cmake-2.8`

    `boost-1.58`

    `cpp-netlib-0.11`
    
    `git`

    On a Ubuntu system, the following command would provide all the dependencies required.

    `apt-get install libboost-dev libcppnetlib-dev cmake git`

* How to compile

    Assuming on a Ubuntu platform:
    
    `git clone https://bitbucket.org/rzhou1984/qget.git`

    `cd qget`

    `mkdir build`

    `cd build`

    `cmake ../`

    `make`

    An executable binary `qget` will be generated under `bin`.

    To run the binary:

    `./bin/qget http://b086782c.bwtest-aws.pravala.com/384MB.jar` will download the first 4MB into a local file named 384MB.jar.

    Check `./bin/qget --help` for more options.

    For example,

    `./bin/qget http://b086782c.bwtest-aws.pravala.com/384MB.jar -j 16 -c 512 -o 16MB.jar` will download the first 16 * 512 KB bytes into
    a local file named `16MB.jar`.

### Who do I talk to? ###

* Please drop me an email if you have any questions.

    zhou.youyi_at_gmail.com
