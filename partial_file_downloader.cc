// -*- mode: c++ -*-

#include <boost/network/protocol/http/client.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <string>
#include <sstream>
#include <iostream>

#include "partial_file_downloader.h"

namespace http = boost::network::http;

partial_file_downloader::partial_file_downloader( const std::string& remote, size_t start, size_t end )
    : m_remote_file( remote )
    , m_start( start )
    , m_end( end )
{
    std::cout << "Creating partial file downloader for " << remote
              << " bytes " << start << " to " << end << std::endl;
}

void partial_file_downloader::start()
{
    m_worker.reset( new boost::thread( boost::bind( &partial_file_downloader::download, this ) ) );
}

void partial_file_downloader::join()
{
    if( m_worker.get() ) m_worker->join();
}

const std::string& partial_file_downloader::content() const
{
    return m_content;
}

void partial_file_downloader::download()
{
    // holds the whole chunk in memory until we write it out
    // may not be a good idea when the file is multiple GB large
    // in which case it would make more sense if we write out
    // each finished chunk to a separate file and assemble
    // them together at last.
    try
    {
        http::client client;
        http::client::request request( m_remote_file );

        std::ostringstream oss;
        oss << "bytes="
            << m_start
            << "-"
            << m_end
            ;

        request << boost::network::header( "Range", oss.str() );

        // may hit 416, some servers can't serve ranged bytes.
        http::client::response response( client.get( request ) );
        m_content = body( response );

        std::cout << "Completed " << m_start << " to " << m_end << std::endl;
    }
    catch ( std::exception& e )
    {
        std::cerr << e.what() << std::endl;
    }
}
