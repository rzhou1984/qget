// -*- mode: c++ -*-

#ifndef PARTIAL_FILE_DOWNLOADER_H
#define PARTIAL_FILE_DOWNLOADER_H

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <string>
#include <sstream>
#include <iostream>

/* A partial file downloder.

   Given a remote URI, retrieve the remote file from the given starting bytes to the give ending bytes,
   inclusive on both ends, using an asynchronized method.
*/
class partial_file_downloader
{
public:
    // Constructs a partial file downloader
    // @remote - the remote file URI
    // @start - starting byte (inclusive)
    // @end - ending byte (inclusize)
    partial_file_downloader( const std::string& remote, size_t start, size_t end );

    // Start downloading thread
    void start();

    // Join the downloading thread, waiting for it to finish
    void join();

    // Get the downloaded content
    const std::string& content() const;

protected:
    // Download the content
    void download();

private:
    boost::shared_ptr< boost::thread > m_worker;

    std::string m_remote_file;
    size_t m_start;
    size_t m_end;

    std::string m_content;
};

typedef boost::shared_ptr< partial_file_downloader > partial_file_downloader_ptr;

#endif
