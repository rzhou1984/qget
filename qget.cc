// -*- mode: c++ -*-

// Part of the following implementation comes from cpp-netlib example:
//   http://cpp-netlib.org/0.12.0/examples/http/simple_wget.html

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#include "file_info.h"
#include "partial_file_downloader.h"

namespace po = boost::program_options;

int main(int argc, char *argv[])
{
    static const size_t KB = 1024;
    static const std::string HTTP = "http://"; // only HTTP protocal is concerned

    std::string remote_file;
    std::string local_file;
    size_t jobs = 4;
    size_t chunk_size = 1024; // in KB

    // parsing all the program options

    po::options_description visible_options;
    po::options_description hidden_options;
    po::positional_options_description positional_options;

    hidden_options.add_options()
        ( "remote", po::value<std::string>( &remote_file ), "Remote file to download" )
        ;
    visible_options.add_options()
        ( "help,h", "Show this message" )
        ( "jobs,j", po::value<size_t>( &jobs )->default_value( 4 ),  "Number of concurrent downloading threads" )
        ( "chunk,c", po::value<size_t>( &chunk_size )->default_value( 1024 ), "Number of KB to download in each job" )
        ( "output,o", po::value<std::string>( &local_file ), "Download to a different file name, by default it uses the remote file name" )
        ;
    positional_options.add( "remote", 1 );

    po::variables_map vm;
    try
    {
        po::store(
            po::command_line_parser( argc, argv ).
            options( po::options_description()
                     .add( visible_options )
                     .add( hidden_options ) )
            .positional( positional_options )
            .run(),
            vm );

        po::notify( vm );
    }
    catch ( po::error& e )
    {
        std::cerr << e.what() << std::endl;
        std::cout << "usage: " << argv[0] << " [options] "
                  << " <remote file>"
                  <<  "\n" "\n"
                  << visible_options;
        std::cout << "\n" "Build time: " << __DATE__ << " " << __TIME__ << std::endl;
        return 1;
    }

    if ( vm.count("help") || remote_file.empty() || jobs == 0 || chunk_size == 0 )
    {
        std::cout << "usage: " << argv[0] << " [options] "
                  << " <remote file>"
                  <<  "\n" "\n"
                  << visible_options;
        std::cout << "\n" "Build time: " << __DATE__ << " " << __TIME__ << std::endl;
        return 0;
    }

    if ( ! boost::iequals( HTTP, remote_file.substr( 0, HTTP.size() ) ) )
    {
        remote_file = HTTP + remote_file;
    }

    std::cout << "Getting file: " << remote_file << std::endl;

    try
    {
        file_info info( remote_file );

        // generate local file name
        std::string filename = local_file;
        if ( filename.empty() )
            filename = info.get_file_name();
        std::cout << "Saving to: " << filename << std::endl;

        // Now fire up all the jobs
        // Ideally there should be thread pool of the given number of threads
        // working on continuously submitted transfer requests, for the simplicity
        // of this task, each chunk will be transferred by a standalone thread.

        std::vector< partial_file_downloader_ptr > pool;

        for ( size_t i = 0; i < jobs; ++i )
        {
            size_t start = i * chunk_size * KB;
            size_t end = start + chunk_size * KB - 1; // might overflow
            if ( info.get_file_size() && end > info.get_file_size() )
                end = info.get_file_size() - 1;
            pool.push_back( partial_file_downloader_ptr( new partial_file_downloader( remote_file, start, end ) ) );

            pool.back()->start();
        }

        std::ofstream ofs( filename.c_str() );

        // all chunks are held in memory until they are written out to disk
        for ( size_t i = 0; i < jobs; ++i )
        {
            pool[i]->join();

            ofs << pool[i]->content();

            pool[i].reset(); // release the chunk
        }

        ofs.close();

        std::cout << "Done. File saved to " << local_file << std::endl;
    }
    catch ( std::exception &e )
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}
